#include <cmath>
#include "simpson.h"
#define pos(x) s+(e-s)*x/steps


using namespace std;


blub integrate(blub s, blub e, int steps, blub (*func)(blub)){
  blub result = 0;
  for (int i = 0; i < steps; i++){
    result += func(pos(i)) + 2 * func((pos(i) + pos((i+1)))/ 2);
  }
  result -= (func(s) - func(e)) / 2;
  return (e-s)*result/ steps / 3;
}
