
#ifndef BLUB
#define BLUB


typedef __float128 blub;

#endif


//blub is blub
blub integrate(blub s, blub e, int steps, blub (*func)(blub));

