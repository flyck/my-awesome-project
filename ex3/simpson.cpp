#include <iostream>
#include <cmath>
#define pos(x) s+(e-s)*x/steps
#define acc 34

typedef __float128 blub;

using namespace std;

__int128 fac[acc];
__int128 tay[acc];

blub sin(blub x){
  
  blub res = 0;
  for (int i = acc - 1; i >= 0; i--){

    res *= x;
    res += (blub)tay[i] / fac[i];
  }
  return res;
}

blub func(blub x){
  return sin(x);
}

blub integrate(blub s, blub e, int steps){
  blub result = 0;
  for (int i = 0; i < steps; i++){
    result += func(pos(i)) + 2 * func((pos(i) + pos((i+1)))/ 2);
  }
  result -= (func(s) - func(e)) / 2;
  return (e-s)*result/ steps / 3;
}

main(){
  __int128 f = 1;
  fac[0] = 1;
  for (int i = 1; i < acc; i++){
    f *= i;
    fac[i] = f;
  }
  for (int i = 0; i < acc; i++){
    tay[i] = (i%4==1) - (i%4==3);
  }
  cout << (long double)integrate(0,M_PI, 1e4) << endl;
  
}
